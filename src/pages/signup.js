import React from "react";
import {
  Button,
  Input,
  IconDash,
  IconEndBracket,
  IconStartBracket
} from "../components/";

export const SignUp = () => {
  // window.location.href = ' www.google.com '
  console.log("hi");

  return (
    <div className="h100 flex flex-col pa-8">
      <div className=" h100 flex flex-col justofy-center items-center">
        <div className="flex justify-center items-center">
          <IconStartBracket />
          <IconDash />
          <IconEndBracket />
        </div>
        <div className="font-lobster c-primary fs-56 lh-70">Boginoo</div>
        <br></br>

        <div className="font-ubuntu bold  mt-5 c-primary fs-32">Бүртгүүлэх</div>
        <div className="mt-5 flex flex-col  ">
          <p className="font-ubuntu fs-16 lh-18 login-ml-24 "> Цахим хаяг</p>
          <Input
            className="login-h-44 fs-20 login-w-381"
            placeholder="name@mail.domain"
            //   name="inputUrl"
            //   onChange={e => setInputUrl(e.target.value)}
          />

          <p className="font-ubuntu fs-16 lh-18 login-ml-24 mt-5"> Нууц үг</p>
          <Input
            className="login-w-381 login-h-44 fs-20"
            placeholder="●●●●●●●●●●"
            //   name="inputUrl"
            //   onChange={e => setInputUrl(e.target.value)}
          />

          <p className="font-ubuntu fs-16 lh-18 login-ml-24 mt-5">
            {" "}
            Нууц үгээ давтана уу{" "}
          </p>
          <Input
            className="login-w-381 login-h-44 fs-20"
            placeholder="●●●●●●●●●●"
            //   name="inputUrl"
            //   onChange={e => setInputUrl(e.target.value)}
          />

          <Button className="font-ubuntu fs-20 lh-23 bold login-w-383 c-default login-h-43 ph-4 b-primary mt-6">
            Бүртгүүлэх
          </Button>
          <br></br>
        </div>
      </div>
    </div>
  );
};
