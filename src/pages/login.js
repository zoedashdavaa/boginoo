import React from "react";
import {
  Button,
  Input,
  IconDash,
  IconEndBracket,
  IconStartBracket
} from "../components/";
import { Link } from "react-router-dom";

export const Login = () => {
  // window.location.href = ' www.google.com '
  console.log("hi");

  return (
    <div className="h100 flex flex-col pa-8">
      <div className=" h100 flex flex-col justofy-center items-center">
        <div className="flex justify-center items-center">
          <IconStartBracket />
          <IconDash />
          <IconEndBracket />
        </div>
        <div className="font-lobster c-primary fs-56 lh-70">Boginoo</div>
        <br></br>

        <div className="font-ubuntu bold mt-5 c-primary fs-32">Нэвтрэх</div>
        <div className="mt-5 flex flex-col  ">
          <p className="font-ubuntu fs-16 lh-18 login-ml-24 "> Цахим хаяг</p>
          <Input
            className="login-h-44 fs-20 login-w-381"
            placeholder="name@mail.domain"
            //   name="inputUrl"
            //   onChange={e => setInputUrl(e.target.value)}
          />

          <p className="font-ubuntu fs-16 lh-18 login-ml-24 mt-5"> Нууц үг</p>
          <Input
            className="login-w-381 login-h-44 fs-20"
            placeholder="●●●●●●●●●●"
            //   name="inputUrl"
            //   onChange={e => setInputUrl(e.target.value)}
          />

          <p className="font-ubuntu fs-16 flex justify-between lh-18 login-w-381">
            <span className="font-ubuntu fs-16 lh-18 c-primary">
              <input type="checkbox" className=""></input>
              Намайг сана
            </span>
            <Link
              to="/forgotpassword"
              className="font-ubuntu fs-16 lh-18 "
              style={{ color: "black" }}
            >
              Нууц үгээ мартсан
            </Link>
          </p>
          <Button className="font-ubuntu fs-20 lh-23 bold login-w-383 c-default login-h-43 ph-4 b-primary mt-4">
            Нэвтрэх
          </Button>
          <br></br>
          <Link
            to="/signup"
            className="font-ubuntu fs-16 lh-18 c-primary flex-center "
          >
            Шинэ хэрэглэгч бол энд дарна уу.
          </Link>
        </div>
      </div>
    </div>
  );
};

// import React, { useEffect } from "react";
// import { useParams } from "react-router-dom";
// import { db } from "../firebase";

// export const UrlRedirect = () => {
//   const { firestoreId } = useParams();
//   console.log(firestoreId);

//   useEffect(() => {
//     const func = async () => {
//       try {
//         let docRef = await db
//           .collection("urls")
//           .doc(`${firestoreId}`)
//           .get();
//         console.log(docRef.data().inputUrl);

//         window.location.href = docRef.data().inputUrl;

//       } catch (error) {
//         alert(error);
//       }
//     };

//     func();
//   }, [firestoreId]);

//   return <h1> This is Url redirect page </h1>;
// };

// import React from "react";

// // import { useHistory, useContext, useState } from "react";

// import {
//   Layout,
//   Button,
//   Input,
//   IconDash,
//   IconEndBracket,
//   IconStartBracket
// } from "../components/";
// // import { useFirebase } from "../firebase";

// export const Login = () => {
//   // const history = useHistory();
//   // const { ready, user } = useContext(AuthContext);
//   // const { auth } = useFirebase();

//   // const [email, setEmail] = useState("");
//   // const [password, setPassword] = useState("");

//   // const handleChangeEmail = e => setEmail(e.target.value);
//   // const handleChangePassword = e => setPassword(e.target.value);
//   // const navigateToSignUp = () => {
//   //   history.push("/signup");
//   // };
//   // const signIn = async () => {
//   //   await auth.signInWithEmailAndPassword(email, password);
//   // };

//   return (
//     <Layout>
//       <div className="h100 flex justify-center">
//         <div className=" form w-8 flex-col justify-center items-center">
//           <div className="flex justify-center items-center">
//             <IconStartBracket />
//             <IconDash />
//             <IconEndBracket />
//           </div>
//           <div className="font-lobster c-primary fs-56 lh-70">Boginoo</div>

//           <div className="mt-5 flex justify-center items-center">
//             <Input
//               className="h-5 w-8"
//               placeholder="https://www.web-huudas.mn"
//             />
//             <Button className="font-ubuntu fs-20 lh-23 bold c-default h-5 ph-4 ml-4 b-primary">
//               Богиносгох
//             </Button>
//           </div>
//         </div>
//       </div>
//     </Layout>
//   );
// };
