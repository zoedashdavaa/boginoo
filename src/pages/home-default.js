import React, { useState } from "react";
import {
  Layout,
  Button,
  Input,
  IconDash,
  IconEndBracket,
  IconStartBracket
} from "../components/";
import { db } from "../firebase";
import { Link } from "react-router-dom";
import { checkIsValidUrl } from "./url-validator";

export const HomeDefault = () => {
  const [inputUrl, setInputUrl] = useState("");
  const [outputUrl, setOutputUrl] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isShortened, setIsShortened] = useState(false);

  const isValidUrl = checkIsValidUrl(inputUrl);

  const handleClick = async () => {
    console.log("hi");
    setIsLoading(true);
    setIsShortened(true);

    const urlDoc = await db
      .collection("urls")
      .add({
        inputUrl
      })
      .catch(error => {
        console.log(error);
      });
    setIsLoading(false);

    setOutputUrl(`${urlDoc.id}`);
  };

  return (
    <div className="h100 flex flex-col pa-8">
      <div className=" h100 flex flex-col justofy-center items-center">
        <div className="flex justify-center items-center">
          <IconStartBracket />
          <IconDash />
          <IconEndBracket />
        </div>
        <div className="font-lobster c-primary fs-56 lh-70">Boginoo</div>

        <div className="mt-5 flex justify-center items-center">
          <Input
            className="h-5 w-8"
            placeholder="https://www.web-huudas.mn"
            name="inputUrl"
            onChange={e => setInputUrl(e.target.value)}
          />
          <Button
            className="font-ubuntu fs-20 lh-23 bold c-default h-5 ph-4 ml-4 b-primary"
            onClick={handleClick}
            disabled={isLoading || !inputUrl || !isValidUrl}
          >
            Богиносгох
          </Button>
        </div>
        {isShortened && (
          <div>
            <div>
              Өгөгдсөн холбоос:
              <div>{inputUrl}</div>
            </div>

            <div>
              Богино холбоос:
              <Link to={outputUrl} target="_blank">
                {outputUrl}
              </Link>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
