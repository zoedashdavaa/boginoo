import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { db } from "../firebase";

export const UrlRedirect = () => {
  const { firestoreId } = useParams();
  console.log(firestoreId);

  useEffect(() => {
    const func = async () => {
      try {
        let docRef = await db
          .collection("urls")
          .doc(`${firestoreId}`)
          .get();
        console.log(docRef.data().inputUrl);

        window.location.href = docRef.data().inputUrl;
      } catch (error) {
        alert(error);
      }
    };

    func();
  }, [firestoreId]);

  return <h1> This is Url redirect page </h1>;
};
