import firebase from "firebase";

const config  = {
    apiKey: "AIzaSyDyb3QFzpFwWUiK1w58Au14Ik2tJFVVnf4",
    authDomain: "boginoo-5870c.firebaseapp.com",
    projectId: "boginoo-5870c",
    storageBucket: "boginoo-5870c.appspot.com",
    messagingSenderId: "470804056940",
    appId: "1:470804056940:web:0e04fec8960b7d7c154149"
  };

firebase.initializeApp(config);

export const db = firebase.firestore();
export const auth = firebase.auth();
export default firebase;
