import React from "react";
import {
  HomeDefault,
  Login,
  SignUp,
  UrlRedirect,
  ForgotPassword
} from "./pages";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./style/main.scss";
import { Layout } from "./components";

const App = () => {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path="/" exact>
            <HomeDefault />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/signup">
            <SignUp />
          </Route>
          <Route path="/forgotpassword">
            <ForgotPassword />
          </Route>

          <Route path="/:firestoreId">
            <UrlRedirect />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
};

export default App;
